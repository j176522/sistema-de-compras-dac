package io.dac.web.controllers;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import io.dac.data.Status;
import io.dac.data.entities.Employee;
import io.dac.data.entities.Order;
import io.dac.data.entities.User;
import io.dac.web.dtos.OrderDto;
import io.dac.web.dtos.UserDto;
import io.dac.web.services.OrderDtoService;
import io.dac.web.services.OrderService;
import io.dac.web.services.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userServ;

	@Autowired
	private OrderService ordServ;

	@Autowired
	private OrderDtoService dtoServ;

	// Get Mappings

	@GetMapping(value = {"/login", "/"})
	public String loginPage(Model model) {
		Map<String,Object> attrs = model.asMap();
		if(attrs.containsKey("error")) {
			return "login-flex";
		}
		return loginView("empty", model);
	}

	@GetMapping("/dashboard")
	public String dashboard(Model model) {
		Map<String, Object> attrs = model.asMap();
		if(!attrs.containsKey("userId")) {
			model.addAttribute("error", "error");
			model.addAttribute("success", null);
			return "redirect:/";
    }
    long userId = (long)attrs.get("userId");
    boolean isEmployee = false;
		if (userServ.findById(userId).get() instanceof Employee) isEmployee = true;
		
		List<OrderDto> dtos = userServ.findUserOrders(userId).stream().map(order -> {
			return dtoServ.convertToDto(order);
		}).collect(Collectors.toList());
		
		model.addAttribute("orders", dtos);
    model.addAttribute("userId", userId);
    model.addAttribute("isEmployee", isEmployee);
    
		return "dashboard-solicitante-flex";
	}

	@GetMapping("/dashboard/{id}")
	public String dashboard(@PathVariable String id, Model model, RedirectAttributes attrs) {
		attrs.addFlashAttribute("userId", Long.parseLong(id));
		return "redirect:/dashboard";
	}

	@GetMapping("user/{userId}/{orderId}")
	public ResponseEntity<byte[]> getBudget(@PathVariable String userId, @PathVariable String orderId,
	Model model){
		long order = Long.parseLong(orderId);
		ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		byte[] doc = ordServ.findById(order).get().getBudget();
		try {
			OutputStream out = new FileOutputStream("pdf.pdf");
			out.write(doc);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
		header.add("Content-Type", "application/pdf");
		// header.add("Content-Length", buffer.toString());
		resp = new ResponseEntity<byte[]>(doc, header, HttpStatus.OK);

		return resp;

	}

	@GetMapping("details/{id}")
	public String details(@PathVariable Long id, Model model) {
		Order order = ordServ.findById(id).get();
		OrderDto dto = dtoServ.convertToDto(order);
		model.addAttribute("order", dto);
		return "details";
	}


	@GetMapping("search")
	public String search(@RequestParam Long userId, @RequestParam String productName,
	@RequestParam boolean isEmployee, Model model) {
		List<Order> orders;
		if(productName.equals(""))
			orders = ordServ.findAll();
		else
			orders = ordServ.findByProductName(productName);
		List<OrderDto> dtos = orders.stream().map(order -> {
			if(isEmployee == true) {
				if(order.getRequester().getId() == userId)
					return dtoServ.convertToDto(order);
				else
					return null;
			} else {
				if(order.getRequester().getDirectBoss().getId() == userId)
					return dtoServ.convertToDto(order);
				else
					return null;
			}
			
		}).filter(order -> order!=null).collect(Collectors.toList());

		model.addAttribute("orders", dtos);
    model.addAttribute("userId", userId);
		model.addAttribute("isEmployee", isEmployee);		
		
		return "dashboard-solicitante-flex";

	}

	@PostMapping("details")
	public String details(Long orderId, Long userId, HttpServletRequest request, Model model) {
		OrderDto dto = dtoServ.convertToDto(ordServ.findById(orderId).get());
		model.addAttribute("order", dto);
		model.addAttribute("userId", userId);
		return "details";
	}

	@PostMapping("/login")
	public String loginPage(UserDto dto, RedirectAttributes attrs, Model model,
	BindingResult result) {
		if(result.hasErrors()) {
			return loginView("error", model);
		}

		User user = userServ.findByUsername(dto.getUsername());

		if(user == null) {
			return loginView("error", model);
		}	else {
			if (dto.getPassword().equals(user.getPassword())) {
				attrs.addFlashAttribute("userId", user.getId());
				return "redirect:/dashboard";
			} else {
				return loginView("error", model);
			}
		}
	}

	@PostMapping("/new-order")
	public String addOrder(Long userId, Model model) {
		model.addAttribute("order", new OrderDto());
		model.addAttribute("userId", userId);
		return "new-order";
	}

	@PostMapping("/new-order/confirmation")
	public String submitOrder(Long userId, OrderDto dto, Model model,
	BindingResult result,	RedirectAttributes attrs) {
		if(result.hasErrors()) {}

		if(dto.getBudgetIn() == null || dto.getBudgetIn().getOriginalFilename().equals(""))
			dto.setBudgetIn(null);
		if(dto.getImageIn() == null || dto.getImageIn().getOriginalFilename().equals(""))
			dto.setImageIn(null);
		if(dto.getJustification() == null || dto.getJustification().equals(""))
			dto.setJustification(null);
		if(dto.getRefLink() == null || dto.getRefLink().equals(""))
			dto.setRefLink(null);
		if(dto.getComments() == null || dto.getComments().equals(""))
			dto.setComments(null);

		Order order = dtoServ.convertToOrder(dto);
    order.setStatus(Status.NOT_REVIEWED);
		ordServ.confirmNewOrder(userId, order);
		attrs.addFlashAttribute("userId", userId);
		return "redirect:/dashboard";
	}

	@PostMapping("/review")
	public String approveOrder(@RequestParam List<Long> selectedOrders,
	@RequestParam String approval, @RequestParam Long userId, Model model,
	RedirectAttributes attrs) {
		List<Order> orders = selectedOrders.stream().map(id -> {
			return ordServ.findById(id).get();
		}).collect(Collectors.toList());
		if (approval.equals("yes")) {
			for(Order order:orders) {
				order.setStatus(Status.APROVED);
				ordServ.updateOrder(order);
			}
		} else {
			for(Order order:orders) {
				order.setStatus(Status.DENIED);
				ordServ.updateOrder(order);
			}
		}
		attrs.addFlashAttribute("userId", userId);
		return "redirect:/dashboard";
	}
	// Auxiliar

	private String loginView(String checks, Model model) {
		if (checks.equals("empty")) {
			model.addAttribute("error", null);
			model.addAttribute("success", null);
		} else if(checks.equals("error")) {
			model.addAttribute("error", "error");
			model.addAttribute("success", null);
		} else {
			model.addAttribute("error", null);
			model.addAttribute("success", "success");
		}

		model.addAttribute("user", new UserDto());
		return "login-flex";
	}

}
