package io.dac.web.errors;

/**
 * UserNotFoundException
 */
public class UserNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 2L;

	public UserNotFoundException() {
		super();
	}

	public UserNotFoundException(String message) {
		super(message);
	}

	public UserNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
}