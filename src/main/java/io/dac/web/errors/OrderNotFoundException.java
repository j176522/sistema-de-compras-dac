package io.dac.web.errors;

/**
 * OrderNotFoundException
 */
public class OrderNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 2L;

	public OrderNotFoundException() {
		super();
	}

	public OrderNotFoundException(String message) {
		super(message);
	}

	public OrderNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
}