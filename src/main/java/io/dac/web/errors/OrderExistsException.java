package io.dac.web.errors;

/**
 * OrderExistsException
 */
public class OrderExistsException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public OrderExistsException() {
		super();
	}

	public OrderExistsException(String message) {
		super(message);
	}

	public OrderExistsException(String message, Throwable cause) {
		super(message, cause);
	}

}