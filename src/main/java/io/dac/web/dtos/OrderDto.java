package io.dac.web.dtos;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

import org.springframework.web.multipart.MultipartFile;

import io.dac.data.OrderType;
import io.dac.data.Status;
import io.dac.data.entities.Order;

/**
 * OrderDto
 */
public class OrderDto {
  
  private long id;

  private String productName;

	private double unitValue;

  private int quantity;

  private LocalDateTime placementTime;
  
  private String viewPlacementTime;

  private Status status;
  
  private OrderType orderType;

  private String refLink;

  private String description;

  private String justification;

	private String comments;
	
  private MultipartFile imageIn;

  private String imageOut;

  private MultipartFile budgetIn;

  private byte[] budgetOut;

  private String requesterName;

  private String requesterDep;

  public OrderDto(){}

  /**
   * 
   * @param order
   */
  public OrderDto(Order order) {
    this.id = order.getId();
    this.productName = order.getProductName();
    this.unitValue = order.getUnitValue();
    this.quantity = order.getQuantity();
    this.placementTime = order.getPlacementTime();
    this.viewPlacementTime = order.getPlacementTime()
      .format(DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss"));
    this.status = order.getStatus();
    this.orderType = order.getOrderType();
    this.refLink = order.getRefLink();
    this.description = order.getDescription();
    this.justification = order.getJustification();
    this.comments = order.getComments();
    this.imageIn = null;
    if (order.getImage() != null)
      this.imageOut = Base64.getEncoder().encodeToString(order.getImage());
    else this.imageOut = null;
    this.budgetIn = null;
    this.budgetOut = order.getBudget();
    this.requesterName = order.getRequester().getName();
    this.requesterDep = order.getRequester().getDepartment().getName();
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }  

  public String getProductName() {
    return this.productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public double getUnitValue() {
    return this.unitValue;
  }

  public void setUnitValue(double unitValue) {
    this.unitValue = unitValue;
  }

  public int getQuantity() {
    return this.quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public LocalDateTime getPlacementTime() {
    return this.placementTime;
  }

  public void setPlacementTime(LocalDateTime placementTime) {
    this.placementTime = placementTime;
  }

  public String getViewPlacementTime() {
    return this.viewPlacementTime;
  }

  public void setPlacementTime(String viewPlacementTime) {
    this.viewPlacementTime = viewPlacementTime;
  }

  public Status getStatus() {
    return this.status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public OrderType getOrderType() {
    return this.orderType;
  }

  public void setOrderType(OrderType orderType) {
    this.orderType = orderType;
  }

  public String getRefLink() {
    return this.refLink;
  }

  public void setRefLink(String refLink) {
    this.refLink = refLink;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getJustification() {
    return this.justification;
  }

  public void setJustification(String justification) {
    this.justification = justification;
  }

  public String getComments() {
    return this.comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public MultipartFile getImageIn() {
    return this.imageIn;
  }

  public void setImageIn(MultipartFile imageIn) {
    this.imageIn = imageIn;
  }

  public String getImageOut() {
    return this.imageOut;
  }

  public void setImageOut(String imageOut) {
    this.imageOut = imageOut;
  }

  public MultipartFile getBudgetIn() {
    return this.budgetIn;
  }

  public void setBudgetIn(MultipartFile budgetIn) {
    this.budgetIn = budgetIn;
  }

  public byte[] getBudgetOut() {
    return this.budgetOut;
  }

  public void setBudgetOut(byte[] budgetOut) {
    this.budgetOut = budgetOut;
  }

  public String getRequesterName() {
    return requesterName;
  }

  public String getRequesterDep() {
    return this.requesterDep;
  }

  public void setRequesterDep(String requesterDep) {
    this.requesterDep = requesterDep;
  }

}