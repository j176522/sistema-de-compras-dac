package io.dac.web.services;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.dac.data.Status;
import io.dac.data.entities.Employee;
import io.dac.data.entities.Order;
import io.dac.data.entities.Supervisor;
import io.dac.data.repositories.OrderRepository;
import io.dac.web.errors.OrderExistsException;
import io.dac.web.errors.OrderNotFoundException;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRep;
	
	@Autowired
	private UserService userServ;

	// Find

	public List<Order> findAll() {
		return orderRep.findAll();
	}

	public Optional<Order> findById(long id) {
		return orderRep.findById(id);
	}

	public List<Order> findByProductName(String productName) {
		return orderRep.findByProductName(productName);
	}

	public List<Order> findReviewedOrders(Status status) {
		return orderRep.findByStatus(status);
	}

	// Add

	public Order addOrder(Order order) {
		if (orderRep.findById(order.getId()).isPresent())
			throw new OrderExistsException();
		return orderRep.save(order);
	}
	
	public Order confirmNewOrder (Long userId, Order order) {
		Employee requester = (Employee)userServ.findById(userId).get();
		Supervisor boss = requester.getDirectBoss();
		order.setPlacementTime(LocalDateTime.now());
		order.setRequester(requester);
		order.setReviewer(boss);
		
		return addOrder(order);
	}

	// Update

	public Order updateOrder(Order order) {
		orderRep.findById(order.getId()).orElseThrow(OrderNotFoundException::new);
		return orderRep.save(order);
	}

	// Delete

	public void deleteOrder(Order order) {
		orderRep.findById(order.getId()).orElseThrow(OrderNotFoundException::new);
		orderRep.delete(order);
	}

	public void deleteOrderById(long id) {
		orderRep.findById(id).orElseThrow(OrderNotFoundException::new);
		orderRep.deleteById(id);
	}
}