package io.dac.web.services;

import java.io.IOException;

import org.springframework.stereotype.Service;

import io.dac.data.entities.Order;
import io.dac.web.dtos.OrderDto;

/**
 * OrderDtoService
 */
@Service
public class OrderDtoService {

  public OrderDto convertToDto(Order order) {

    OrderDto dto = new OrderDto(order);

    return dto;
  }

  public Order convertToOrder(OrderDto dto) {

    Order order = new Order(dto.getProductName(), dto.getUnitValue(),
    dto.getQuantity(), null, null, dto.getDescription());

    order.setPlacementTime(dto.getPlacementTime());
    order.setOrderType(dto.getOrderType());
    order.setRefLink(dto.getRefLink());
    order.setJustification(dto.getJustification());
    order.setComments(dto.getComments());
    try {
      if(dto.getImageIn() != null)
        order.setImage(dto.getImageIn().getBytes());
      else 
      order.setImage(null);
      if(dto.getBudgetIn() != null)
        order.setBudget(dto.getBudgetIn().getBytes());
      else
        order.setBudget(null);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return order;

  }
}