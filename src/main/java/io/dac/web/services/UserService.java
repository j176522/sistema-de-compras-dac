package io.dac.web.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.dac.data.entities.Employee;
import io.dac.data.entities.Order;
import io.dac.data.entities.Supervisor;
import io.dac.data.entities.User;
import io.dac.data.repositories.UserRepository;
import io.dac.web.errors.UserExistsException;
import io.dac.web.errors.UserNotFoundException;

@Service
public class UserService{

	@Autowired
	private UserRepository userRep;

	// Find

	public List<User> findAdll(){
		return userRep.findAll();
	}

	public List<User> findByName(String name) {
		return userRep.findByName(name);
	}

	public User findByUsername(String username) {
		return userRep.findByUsername(username);
	}

	public Optional<User> findById(long id) {
		return userRep.findById(id);
	}
	
	public List<Order> findUserOrders(long id) {
		User user = findById(id).get();
		if (user instanceof Employee) {
			Employee emp = (Employee)user;
			return emp.getOrdersMade();
		} else if (user instanceof Supervisor) {
			Supervisor sup = (Supervisor)user;
			return sup.getOrdersToReview();
		} else {
			return null;
		}
	}

	// Add

	public User addUser(User user) {
		if (userRep.findById(user.getId()).isPresent())
			throw new UserExistsException();
		return userRep.save(user);
	}

	// Update

	public User updateUser(User user) {
		userRep.findById(user.getId()).orElseThrow(UserNotFoundException::new);
		return userRep.save(user);
	}

	// Delete
	
	public void deleteUser(User user) {
		userRep.findById(user.getId()).orElseThrow(UserNotFoundException::new);
		userRep.delete(user);
	}

	public void deleteUserById(long id) {
		userRep.findById(id).orElseThrow(UserNotFoundException::new);
		userRep.deleteById(id);
	}
	
}