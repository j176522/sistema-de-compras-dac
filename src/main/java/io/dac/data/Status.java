package io.dac.data;

public enum Status {
	
	NOT_REVIEWED, APROVED, DENIED, PLACING, PLACED;
	
}
