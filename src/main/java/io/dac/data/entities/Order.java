package io.dac.data.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.dac.data.OrderType;
import io.dac.data.Status;

@Entity
@Table(name = "order_table")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  
  @NotNull
	private String productName;

  @NotNull
	private double unitValue;

  @NotNull
	private int quantity;

  @NotNull
	private LocalDateTime placementTime;

  @NotNull
  private Status status;
  
  @NotNull
  private OrderType orderType;

	private String refLink;
	
	@NotNull
	private String description;

  private String justification;

	private String comments;
	
	@Lob
	private byte[] image;

	@Lob
	private byte[] budget;

  @NotNull
	@ManyToOne
	@JoinColumn(name = "requester")
	@JsonManagedReference("requester")
	private Employee requester;

  @NotNull
	@ManyToOne
	@JoinColumn(name = "reviewer")
	@JsonManagedReference("reviewer")
	private Supervisor reviewer;

	// Constructors

	public Order() {}

	/**
	 * 
	 * @param productName
	 * @param unitValue
	 * @param quantity
	 * @param requester
	 * @param orderType
	 */
	public Order(String productName, double unitValue, int quantity,
	Employee requester, OrderType orderType, String description) {
		this.productName = productName;
		this.unitValue = unitValue;
		this.quantity = quantity;
		this.requester = requester;
    this.status = Status.NOT_REVIEWED;
    this.orderType = orderType;
		this.placementTime = LocalDateTime.now();
		this.description = description;
	}

	public static boolean createOrder(Order order) {
		return true;
	}
 
	// Getters and Setters

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getUnitValue() {
		return this.unitValue;
	}

	public void setUnitValue(double unitValue) {
		this.unitValue = unitValue;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public LocalDateTime getPlacementTime() {
		return this.placementTime;
	}

	public void setPlacementTime(LocalDateTime placementTime) {
		this.placementTime = placementTime;
	}

	public Status getStatus() {
		return this.status;
	}
	
	public void setStatus(Status status) {
		this.status = status;
  }
  
  public OrderType getOrderType() {
    return this.orderType;
  }


  public String getRefLink() {
    return this.refLink;
  }

  public void setRefLink(String refLink) {
    this.refLink = refLink;
	}
	
	public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getJustification() {
    return this.justification;
  }

  public void setJustification(String justification) {
    this.justification = justification;
  }

  public String getComments() {
    return this.comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public byte[] getBudget() {
		return this.budget;
	}

	public void setBudget(byte[] budget) {
		this.budget = budget;
	}

  public void setOrderType(OrderType orderType){
    this.orderType = orderType;
  }

	public Employee getRequester() {
		return this.requester;
	}

	public void setRequester(Employee requester) {
		this.requester = requester;
	}

	public Supervisor getReviewer() {
		return this.reviewer;
	}

	public void setReviewer(Supervisor reviewer) {
		this.reviewer = reviewer;
	}

}