package io.dac.data.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Employee extends User{

	@ManyToOne
	@JoinColumn
	@JsonManagedReference("boss")
	private Supervisor directBoss;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "requester")
	@JsonBackReference("requester")
	private List<Order> ordersMade;

	// Constructors

	public Employee() {}

	public Employee(Supervisor directBoss) {
		this.directBoss = directBoss;
	}

	public Employee(String name, String username, String pwd, Department department) {
		super(name, username, pwd, department);
	}

	public Employee(String name, String username, String pwd, Department department,
	Supervisor directBoss) {
		super(name, username, pwd, department);
		this.directBoss = directBoss;
	}

	public Employee(String name, String username, String pwd, Department department,
	Supervisor directBoss, List<Order> ordersMade) {
		super(name, username, pwd, department);
		this.directBoss = directBoss;
		this.ordersMade = ordersMade;
	}

	// Getters and Setters

	@Override
	public String getType() {
		return "Employee";
	}

	public Supervisor getDirectBoss() {
		return this.directBoss;
	}

	public void setDirectBoss(Supervisor directBoss) {
		this.directBoss = directBoss;
	}

	public List<Order> getOrdersMade() {
		return this.ordersMade;
	}

	public void setOrdersMade(List<Order> ordersMade) {
		this.ordersMade = ordersMade;
	}


}