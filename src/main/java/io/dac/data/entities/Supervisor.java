package io.dac.data.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Supervisor extends User{

	private int clearanceLevel;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "directBoss")
	@JsonBackReference("boss")
	private List<Employee> subordinates;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "reviewer")
	@JsonBackReference("reviewer")
	private List<Order> ordersToReview;

	// Constructors

	public Supervisor() {}

	public Supervisor(int clearanceLevel) {
		super();
		this.clearanceLevel = clearanceLevel;
	}

	public Supervisor(String name, String username, String pwd, Department department) {
		super(name, username, pwd, department);
	}

	public Supervisor(String name, String username, String pwd, Department department,
	int clearanceLevel) {
		super(name, username, pwd, department);
		this.clearanceLevel = clearanceLevel;
	}

	public Supervisor(String name, String username, String pwd, Department department,
	int clearanceLevel,	List<Order> ordersToReview) {
		super(name, username, pwd, department);
		this.clearanceLevel = clearanceLevel;
		this.ordersToReview = ordersToReview;
	}

	// Getters and Setters

	@Override
	public String getType() {
		return "Supervisor";
	}

	public int getClearanceLevel() {
		return this.clearanceLevel;
	}

	public void setClearanceLevel(int clearanceLevel) {
		this.clearanceLevel = clearanceLevel;
	}

	public List<Order> getOrdersToReview() {
		return this.ordersToReview;
	}

	public void setOrdersToReview(List<Order> ordersToReview) {
		this.ordersToReview = ordersToReview;
	}
	
}