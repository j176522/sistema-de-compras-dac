package io.dac.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import io.dac.data.entities.Department;
import io.dac.data.entities.Employee;
import io.dac.data.entities.Order;
import io.dac.data.entities.Supervisor;
import io.dac.data.repositories.DepartmentRepository;
import io.dac.data.repositories.OrderRepository;
import io.dac.data.repositories.UserRepository;

/**
 * DataLoader
 */
@Component
public class DataLoader implements ApplicationRunner{

	@Autowired
	private DepartmentRepository depRep;

	@Autowired
	private UserRepository userRep;

	@Autowired
	private OrderRepository orderRep;

	public void run(ApplicationArguments args) {

		Department compras = new Department("Compras", "CMP");
		Department ti = new Department("Tecnologia", "TI");
		compras = depRep.save(compras);
		ti = depRep.save(ti);

		Supervisor supervisor = new Supervisor("Marcelo Furioso", "furioso", "furioso", ti, 1);
		supervisor = userRep.save(supervisor);
		Employee employee1 = new Employee("Jonathas", "jow", "jow", ti, supervisor);
		employee1 = userRep.save(employee1);
		Employee employee2 = new Employee("Breno", "breno", "breno", ti, supervisor);
		employee2 = userRep.save(employee2);
		
    Order order1 = new Order("Caneta", 2.5, 10, employee1, OrderType.PRODUCT, "Caneta Transparente Bic de tinta preta");
    order1.setOrderType(OrderType.PRODUCT);
    order1.setReviewer(supervisor);
		order1 = orderRep.save(order1);

    order1 = new Order("Televisão 4K", 8000, 1, employee1, OrderType.PRODUCT, "Televisão 4K Samsung de 42 polegadas");
		order1.setReviewer(supervisor);
		order1 = orderRep.save(order1);

    order1 = new Order("Mesa", 600, 1, employee2, OrderType.PRODUCT, "Mesa de madeira de 6 lugares");
    order1.setOrderType(OrderType.PRODUCT);
		order1.setReviewer(supervisor);
		order1 = orderRep.save(order1);
				
	}
	
}