package io.dac.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.dac.data.Status;
import io.dac.data.entities.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{

	List<Order> findByProductName(String productName);
	List<Order> findByStatus(Status status);
	
}