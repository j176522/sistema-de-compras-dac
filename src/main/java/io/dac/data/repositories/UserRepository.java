package io.dac.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.dac.data.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	List<User> findByName(String name);
	User findByUsername(String username);
}