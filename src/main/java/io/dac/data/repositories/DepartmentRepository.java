package io.dac.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.dac.data.entities.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long>{

	Department findByName(String name);
	Department findByAcronym(String acronym);

}