package io.dac.data;

/**
 * OrderType
 */
public enum OrderType {

  PRODUCT, SERVICE, PRODUCT_AND_SERVICE;
  
}