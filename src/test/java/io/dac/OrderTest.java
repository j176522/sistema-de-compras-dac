package io.dac;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import io.dac.data.OrderType;
import io.dac.data.Status;
import io.dac.data.entities.Order;

/**
 * OrderTest
 */
public class OrderTest {

  @Test
  public void testOrder() {
    Order order = new Order();
    order.setOrderType(OrderType.PRODUCT);
    order.setProductName("Lapis");
    order.setQuantity(10);
    order.setUnitValue(2.50);
    order.setStatus(Status.NOT_REVIEWED);
    assertEquals(true, Order.createOrder(order));
  }
  
}