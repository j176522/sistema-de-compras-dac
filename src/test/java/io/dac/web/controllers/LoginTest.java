package io.dac.web.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * LoginTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LoginTest {

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mvc;

  @Before
    public void setup() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mvc = builder.build();
    }

  @Test
  public void correctLogin() throws Exception {
    

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content("username=jow&password=jow");

    this.mvc.perform(builder)
    .andExpect(MockMvcResultMatchers.redirectedUrl("/dashboard"));
  }

  @Test
  public void wrongUsernameAndPassword() throws Exception {

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content("username=jowjow&password=jowjow");

    this.mvc.perform(builder)
    .andExpect(MockMvcResultMatchers.view().name("login-flex"));

  }

  @Test
  public void wrongPassword() throws Exception {

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content("username=jow&password=jowjow");

    this.mvc.perform(builder)
    .andExpect(MockMvcResultMatchers.view().name("login-flex"));
  }

  @Test
  public void wrongUsername() throws Exception {

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content("username=jowjow&password=jow");

    this.mvc.perform(builder)
    .andExpect(MockMvcResultMatchers.view().name("login-flex"));

  }

}