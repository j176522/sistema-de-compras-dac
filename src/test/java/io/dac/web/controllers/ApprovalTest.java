package io.dac.web.controllers;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import io.dac.data.Status;
import io.dac.data.repositories.OrderRepository;

/**
 * ApprovalTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApprovalTest {

  @Autowired
  private WebApplicationContext wac;

  @Autowired
  private OrderRepository ordRep;

  private MockMvc mvc;

  @Before
  public void setup() {
      DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
      this.mvc = builder.build();
  }

  @Test
  public void testAccept() throws Exception{

    String content = "selectedOrders=7,8&approval=yes&userId=4";

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/review")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content(content);

    this.mvc.perform(builder)
    .andExpect(MockMvcResultMatchers.redirectedUrl("/dashboard"));

    assertEquals(true, ordRep.findById(7L).get().getStatus().equals(Status.APROVED));

  }
  
  @Test
  public void testDeny() throws Exception{

    String content = "selectedOrders=7,8&approval=no&userId=4";

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/review")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content(content);

    this.mvc.perform(builder)
    .andExpect(MockMvcResultMatchers.redirectedUrl("/dashboard"));

    // assertEquals(true, ordRep.findById(7L).get().getStatus().equals(Status.APROVED));

  }

  @Test
  public void noSelected() throws Exception{

    String content = "approval=no&userId=4";

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/review")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content(content);

    try{
      this.mvc.perform(builder)
        .andExpect(MockMvcResultMatchers.status().is4xxClientError())
        .andExpect(MockMvcResultMatchers.status().reason("Required List parameter 'selectedOrders' is not present"));
    } catch (Exception e) {
      e.printStackTrace();
    }


  }

}