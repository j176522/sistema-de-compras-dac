package io.dac.web.controllers;

import static org.junit.Assert.assertEquals;

import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import io.dac.data.repositories.OrderRepository;

/**
 * NewOrderTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class NewOrderTest {

  @Autowired
  private WebApplicationContext wac;

  @Autowired
  private OrderRepository ordRep;

  private MockMvc mvc;

  @Before
  public void setup() {
      DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
      this.mvc = builder.build();

      MockitoAnnotations.initMocks(this);

  }

  @Test
  public void newOrder() throws Exception{

    String content = "userId=5&productName=Borracha&orderType=PRODUCT&description=CanetaBic&quantity=10&unitValue=1";

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/new-order/confirmation")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content(content);

    this.mvc.perform(builder)
    .andExpect(MockMvcResultMatchers.redirectedUrl("/dashboard"));

    assertEquals(false, ordRep.findByProductName("Borracha").isEmpty());
  }

  @Test
  public void noProductName() throws Exception{

    String content = "userId=5&orderType=PRODUCT&description=CanetaBic&quantity=10&unitValue=1";

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/new-order/confirmation")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content(content);

    try{
      this.mvc.perform(builder)
        .andExpect(MockMvcResultMatchers.status().is4xxClientError())
        // .andExpect(MockMvcResultMatchers.status().)
        .andDo(MockMvcResultHandlers.print());
    } catch (NestedServletException e) {
      assertEquals(true, e.getCause().getCause().getCause().getClass().equals(ConstraintViolationException.class));
    }

    assertEquals(true, ordRep.findByProductName("Borracha").isEmpty());

  }

  @Test
  public void noOrderType() throws Exception{

    String content = "userId=5&productName=Borracha&description=CanetaBic&quantity=10&unitValue=1";

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/new-order/confirmation")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content(content);

    try{
      this.mvc.perform(builder)
        .andExpect(MockMvcResultMatchers.status().is4xxClientError())
        // .andExpect(MockMvcResultMatchers.status().)
        .andDo(MockMvcResultHandlers.print());
    } catch (NestedServletException e) {
      System.out.println("\n\nOrder\n\n");
      e.printStackTrace();
      assertEquals(true, e.getCause().getCause().getCause().getClass().equals(ConstraintViolationException.class));
    }

    assertEquals(true, ordRep.findByProductName("Borracha").isEmpty());

  }

  @Test
  public void noDescription() throws Exception{

    String content = "userId=5&productName=Borracha&orderType=PRODUCT&quantity=10&unitValue=1";

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/new-order/confirmation")
      .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
      .accept(MediaType.APPLICATION_FORM_URLENCODED)
      .characterEncoding("UTF-8")
      .content(content);

    try{
      this.mvc.perform(builder)
        .andExpect(MockMvcResultMatchers.status().is4xxClientError())
        // .andExpect(MockMvcResultMatchers.status().)
        .andDo(MockMvcResultHandlers.print());
    } catch (NestedServletException e) {
      assertEquals(true, e.getCause().getCause().getCause().getClass().equals(ConstraintViolationException.class));
    }

    assertEquals(true, ordRep.findByProductName("Borracha").isEmpty());

  }

//   @Test
//   public void noQuantity() throws Exception{

//     String content = "userId=5&productName=Borracha&orderType=PRODUCT&description=CanetaBic&unitValue=1";

//     MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/new-order/confirmation")
//       .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
//       .accept(MediaType.APPLICATION_FORM_URLENCODED)
//       .characterEncoding("UTF-8")
//       .content(content);

//     try{
//       this.mvc.perform(builder)
//         .andExpect(MockMvcResultMatchers.status().is4xxClientError())
//         // .andExpect(MockMvcResultMatchers.status().)
//         .andDo(MockMvcResultHandlers.print());
//     } catch (NestedServletException e) {
//       System.out.println("\n\nQuantity\n\n");
//       e.printStackTrace();
//       assertEquals(true, e.getCause().getCause().getCause().getClass().equals(ConstraintViolationException.class));
//     }

//     assertEquals(true, ordRep.findByProductName("Borracha").isEmpty());

//   }

//   @Test
//   public void noUnitValue() throws Exception{

//     String content = "userId=5&productName=Borracha&orderType=PRODUCT&description=CanetaBic&quantity=10";

//     MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/new-order/confirmation")
//       .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
//       .accept(MediaType.APPLICATION_FORM_URLENCODED)
//       .characterEncoding("UTF-8")
//       .content(content);

//     try{
//       this.mvc.perform(builder)
//         .andExpect(MockMvcResultMatchers.status().is4xxClientError())
//         // .andExpect(MockMvcResultMatchers.status().)
//         .andDo(MockMvcResultHandlers.print());
//     } catch (NestedServletException e) {
//       System.out.println("\n\nValue\n\n");
//       e.printStackTrace();
//       assertEquals(true, e.getCause().getCause().getCause().getClass().equals(ConstraintViolationException.class));
//     }

//     assertEquals(true, ordRep.findByProductName("Borracha").isEmpty());

//   }
  
}