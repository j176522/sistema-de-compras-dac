# ESTRATÉGIAS DE TESTES

Estratégias pensadas para testar o sistema de compras da DAC.

## Nível do Teste
  1. Testes de Unidade
		* Usaremos JUnit para testar as camadas de cada serviço e os serviços separadamente
  2. Testes Integração
		* Usaremos JUnit para testar a integração entre os serviços
  3. Testes de Sistema
  4. Testes de Aceitação
		* Teste programado para fim da próxima sprint, quando os 3 serviços devem estar prontos

## Tipo do Teste
  1. Teste Funcional
		* Exemplos de teste são:
			* Testar se supervisor consegue fazer solicitação (não deve conseguir)
			* Testar se solicitação chega ao responsável por compras apenas após a aprovação do supervisor
			* Testar feedback de aprovação ao funcionário
  2. Teste de Usabilidade
		* Testes abrangirão a usabilidade do sistema, como formulário não destrutivo no caso de erro na realização de solicitação, mensagens claras de falha e sucesso e feedback de estado de execução do sistema

## Técnica do Teste
  1. Teste Funcional ou Caixa Preta
  2. Teste Baseado em Defeito
