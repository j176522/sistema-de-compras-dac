# Sistema de Compras DAC

Sistema para auxiliar o processo de compras da Diretoria Acadêmica da Unicamp  

## Testando o sistema

Dado que não possuo domínios para realizar o deploy do site o teste resume-se ao teste local, que encontra-se explicado a seguir.

Existem duas formas de se testar o sistema localmente. A primeira é baixando o jar nesse [link](https://drive.google.com/file/d/1FmGO5asXgbLFednhP8IdePDRG-fohqgg/view?usp=sharing) e executando com `java -jar /path/to/jar`, em que `/path/to/jar` é o caminho para o arquivo na sua máquina. A segunda é clonando o repositório e rodando a aplicação com o Maven. Para isso basta executar `mvn spring-boot:run` na raiz do projeto.

### Importante

Para testar o sistema existem 3 usuários (a parte de criação de usuários não foi implementada dado o tempo e considerando que esses seriam inseridos pela DAC).
  1. Empregado 1:

    login: jow
    senha: jow
  2. Empregado 2:

    login: breno
    senha: breno
  3. Supervisor:
  
    login: furioso
    senha: furioso

## Arquitetura do Sistema  

![Arquitetura do sistema](arq.png)

### Estilos arquiteturais e seus componentes

#### Estilos
A arquitetura do sistema é composta por uma combinação de dois estilos arquiteturais:  
  * Arquitetura em Camadas  
  * Arquitetura Orientada a Serviços  

Cada componente de Serviço será arquiteturado em camadas próprias (mesmo que parecidas).

#### Componentes
Os componentes de Serviço serão 3:  
  1. Serviço de requisitante  
  2. Serviço de supervisor  
  3. Serviço de responsável por compras  

O serviço de requisitante será responsável por enviar pedidos ao serviço de supervisor, realizar alterações em algum pedido de acordo com a requisição do supervisor ou do responsável por compras e por exibir os pedidos para o usuário.  
O serviço de supervisor será responsável por aprovar, negar ou pedir alterações nos pedidos do requisitante.
O serviço de responsável por compras será responsável por finalizar o processo de compras e preenchimento dos formulários e de pedir alterações para o requisitante.

Por mais que separados em grãos muito gerais, a manutenção do sistema se dará dentro das camadas de cada serviço facilitando esse trabalho.  

Os componentes das Camadas serão 2:  
  1. Camada de gerenciamento de requisições (baseada em API REST)  
  2. Camada de persistência de dados  

Uma camada que seria de visão (ou algum tipo de front-end) foi incluída na camada de gerenciamento de requisições visto que a própria framework utilizada trata e gerencia as visões a partir das requisições REST.

### Subcomponentes

#### Subcomponentes da camada de gerenciamento de requisições
A camada de gerenciamento de requisições será divida em subcamadas. As subcamadas são as utilizadas de praxe em API's REST:
  1. Camada de controladores
  2. Camada de serviços
  3. Camada de repositórios

Os controladores serão os responsáveis por mapear as requisições HTTP em determinados URI's a funções que chamam serviços e, baseados nas respostas dos serviços, eles responderão às requisições com alguma visão.  
Os serviços serão os responsáveis por se conectar aos repositórios e buscar, alterar, criar, deletar, autenticar (e etc.) algum usuário ou pedido.  
Os repositórios formam a subcamada que se conecta à camada de persistência de dados e que recebendo as requisições dos serviços alteram os dados persistidos do sistema.
