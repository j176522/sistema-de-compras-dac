* @j176522
  * Arquitetação do sistema
  * Camada de gerenciamento de requisições e suas subcamadas
    * Registro e autenticação de usuários
  * Camada de persistencia de dados
  * 
  
* @eduardo.yuji na Sprint 2
  * Construção do wireframe (dashboard, página de login e formulário submissão)
  * Teste do protótipo (wireframe) com a DAC. 
     * [wireframe](https://www.figma.com/file/C6Wdvw3a1LKhAkw69C5v9fLL/EngSoftware?node-id=0%3A1)
  * Construção parcial do front-end (CSS e HTML do Figma e Zeplin)

* @mdanilor na Sprint 2
  * Implementação do LDAP 
